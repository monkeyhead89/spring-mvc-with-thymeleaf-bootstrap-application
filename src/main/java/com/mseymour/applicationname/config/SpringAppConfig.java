package com.mseymour.applicationname.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by mattseymour on 15/03/2017.
 */

@Configuration
@ComponentScan({"com.mseymour.applicationname"})
public class SpringAppConfig {
}
