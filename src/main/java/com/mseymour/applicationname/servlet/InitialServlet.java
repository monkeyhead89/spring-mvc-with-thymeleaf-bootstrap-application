package com.mseymour.applicationname.servlet;

import com.mseymour.applicationname.config.SpringAppConfig;
import com.mseymour.applicationname.config.SpringServletConfig;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 * Created by mattseymour on 15/03/2017.
 */
public class InitialServlet extends AbstractAnnotationConfigDispatcherServletInitializer {

        @Override
        protected Class<?>[] getRootConfigClasses() {
            return new Class[] { SpringAppConfig.class };
        }

        @Override
        protected Class<?>[] getServletConfigClasses() {
            return new Class[] { SpringServletConfig.class };
        }

        @Override
        protected String[] getServletMappings() {
            return new String[] { "/" };
        }


}
